#ifndef CITIZEN_H
#define CITIZEN_H

#include <QGraphicsEllipseItem>
#include <QtMath>

#include <QRandomGenerator>
//class QRandomGenerator;

enum class Status { fit, infected, dead};

class Citizen : public QGraphicsEllipseItem
{
public:
    explicit Citizen(QGraphicsItem *parent = nullptr);

    static void initializeCitizen();

    void setRecoveryTime(quint64 time);
    void giveVirus(Citizen* potentialInfector);

    bool isDead() const;
    bool isInfected() const;

    void update();

private:
    void initializeValues();
    void initializeDirection();
    void initializeSpeed();
    void initializeStatus();

    void updateColor();
    void updatePosition();
    void updateStatus();
    void updateSpeed();
    void updateDirection();

    void recover();

    QPointF calculateNextPoint(const QPointF& start) const;
    void setPosition(const QPointF& pos);

    void jumpWest(const QPointF& pos);
    void jumpEast(const QPointF& pos);
    void jumpNorth(const QPointF& pos);
    void jumpSouth(const QPointF& pos);

    inline static QRandomGenerator* _randomGenerator  = QRandomGenerator::global();

    // movement
    qreal _speed = 0.0;
    qreal _direction = M_PI_2; // π / 2

    // Status
    Status  _status {Status::fit};
    quint64 _infectedTime = 0;
    quint64 _lifeCounter = 0;

    // static
    inline static quint64 _recoveryTime = 20;
    inline static qreal   _maxSpeed = 5.0;
    inline static qreal   _minSpeed = 0.;
    inline static qreal   _initialInfectionRate = 0.1;
    inline static qreal   _diameter = 8.0;
};

#endif // CITIZEN_H

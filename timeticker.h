#ifndef TIMETICKER_H
#define TIMETICKER_H

#include <QObject>

class TimeTicker : public QObject
{
    Q_OBJECT
public:
    explicit TimeTicker(QObject *parent = nullptr);

    quint64 ticks() const;

    void start();
    void stop();

signals:
    void tick();

protected:

    void timerEvent(QTimerEvent *event) override;

private:
    int _timerId;
    quint64 _count = 0;

};

#endif // TIMETICKER_H

#ifndef WORLDSCENE_H
#define WORLDSCENE_H

#include <QGraphicsScene>

class Citizen;
class TimeTicker;

using CitizenList = QList<Citizen*>;

class WorldScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit WorldScene(QObject* parent);
    void initialSpreadOfCitizen(int number, int lines = 1);

    quint32 deadCitizen() const;
    quint32 infectedCitizen() const;

public slots:
    void run(bool value);

private slots:
    void changeOnTick();

signals:
    void deathsChanged(int number);
    void infectedChanged(int number);

private:
    CitizenList citizen() const;

    void clear();
    void establishConnections();
    void moveItem(Citizen* item);
    void interact(Citizen* item);

    TimeTicker* _clock{nullptr};
};

#endif // WORLDSCENE_H

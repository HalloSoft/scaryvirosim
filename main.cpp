#include "widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString versionString = QString("%1.%2.%3.%4")
            .arg(VERSION_MAJOR)
            .arg(VERSION_MINOR)
            .arg(VERSION_REVISION)
            .arg(VERSION_BUILD);
    a.setApplicationVersion(versionString);

    QCoreApplication::setOrganizationName("Scary Hallosoft");
    QCoreApplication::setOrganizationDomain("ScaryHallosoft.de");
    QCoreApplication::setApplicationName("ScaryViroSim");

    Widget w;
    w.showMaximized();
    return a.exec();
}

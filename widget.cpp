#include "widget.h"
#include "ui_widget.h"

#include "worldscene.h"
#include "countvisuwidget.h"
#include "citizen.h"

#include <QSettings>


Widget::Widget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::Widget),
      _world(new WorldScene(this))
{
    ui->setupUi(this);

    readSettings();

    ui->graphicsView->setScene(_world);
    ui->graphicsView->setSceneRect(_world->sceneRect());

    QString title = qApp->applicationVersion();
    setWindowTitle(QString("Viro-Sim %1").arg(title));

    reset();

    ui->infectedVisuWidget->setColor(Qt::red);

    initializeConnections();

}

Widget::~Widget()
{
    delete ui;
}

void Widget::closeEvent(QCloseEvent *)
{
    saveSettings();
}

void Widget::setDeathsInfo(int number)
{
    ui->labelNoOfDeaths->setText(QString::number(number));
}

void Widget::setInfectedInfo(int number)
{
    ui->labelNoOfInfected->setText(QString::number(number));
}

void Widget::reset()
{
    Citizen::initializeCitizen();

    _world->initialSpreadOfCitizen(_initialColumns, _initialLines);
    ui->deathVisuWidget->reset();
    ui->infectedVisuWidget->reset();
}

void Widget::initializeConnections()
{
    bool isConnected = false; Q_UNUSED(isConnected);
    isConnected = connect(_world, &WorldScene::deathsChanged, this, &Widget::setDeathsInfo);
    Q_ASSERT(isConnected);

    isConnected = connect(_world, &WorldScene::deathsChanged, ui->deathVisuWidget, &CountVisuWidget::appendValue);
    Q_ASSERT(isConnected);

    isConnected = connect(_world, &WorldScene::infectedChanged, this, &Widget::setInfectedInfo);
    Q_ASSERT(isConnected);

    isConnected = connect(_world, &WorldScene::infectedChanged, ui->infectedVisuWidget, &CountVisuWidget::appendValue);
    Q_ASSERT(isConnected);

    isConnected = connect(ui->buttonStartStop, &QPushButton::clicked, _world, &WorldScene::run);
    Q_ASSERT(isConnected);

    isConnected = connect(ui->buttonReset, &QPushButton::clicked, this, &Widget::reset);
    Q_ASSERT(isConnected);
}

void Widget::saveSettings() const
{
    QSettings settings;
    settings.setValue("initialcolumns", _initialColumns);
    settings.setValue("initiallines", _initialLines);
}

void Widget::readSettings()
{
    QSettings settings;
    _initialColumns = settings.value("initialcolumns", _initialColumns).toInt();
    _initialLines = settings.value("initiallines", _initialLines).toInt();
}


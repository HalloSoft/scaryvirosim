#include "citizen.h"

#include <algorithm>
#include <QBrush>
#include <QGraphicsScene>
#include <QRandomGenerator>
#include <QSettings>

#include <QDebug>

using namespace std;

//QRandomGenerator Citizen::_randomGenerator = QRandomGenerator::global();

Citizen::Citizen(QGraphicsItem *parent) :
    QGraphicsEllipseItem(parent)/*,
    _randomGenerator(QRandomGenerator::global())*/
{
    initializeStatus();
    initializeDirection();
    initializeSpeed();

    setRect(0,0,_diameter,_diameter);
    setBrush(QBrush(Qt::green));
}

void Citizen::initializeCitizen() // static
{
    QSettings settings;
    settings.beginGroup("Citizen");
    _maxSpeed = settings.value("maxspeed", _maxSpeed).toReal();
    _minSpeed = settings.value("minspeed", _minSpeed).toReal();
    _initialInfectionRate = settings.value("initialinfectionrate", _initialInfectionRate).toReal();
    _recoveryTime = settings.value("recoverytime", _recoveryTime).toULongLong();

    qDebug() << "Initializing citizen with:";
    qDebug() << "maxspeed" << _maxSpeed;
    qDebug() << "minspeed" << _minSpeed;
    qDebug() << "initialinfectionrate" << _initialInfectionRate;
    qDebug() << "recoverytime" << _recoveryTime;

}

void Citizen::giveVirus(Citizen* potentialInfector)
{
    if(_status != Status::dead)
    {
        if(potentialInfector->_status == Status::infected)
            _status = Status::infected;
    }
}

bool Citizen::isDead() const
{
    return _status == Status::dead;
}

bool Citizen::isInfected() const
{
    return _status == Status::infected;
}

void Citizen::update()
{
    if(_status != Status::dead)
    {
        updateStatus();
        updateSpeed();
        updateDirection();
        updatePosition();
    }

    updateColor();

    ++_lifeCounter;
}

void Citizen::initializeValues()
{
    QSettings settings;
    _speed = settings.value("initialspeed", _speed).toReal();
}

void Citizen::initializeDirection()
{
    _direction = _randomGenerator->bounded(M_PI * 2);
}

void Citizen::initializeSpeed()
{
    _speed = (_minSpeed + _maxSpeed) / 2;
}

void Citizen::initializeStatus()
{
    if(_randomGenerator->bounded(1.0) < _initialInfectionRate)
        _status = Status::infected;
    else
        _status = Status::fit;
}

void Citizen::updateColor()
{
    QColor currentColor;
    if(_status == Status::fit)      currentColor = Qt::green;
    if(_status == Status::infected) currentColor = Qt::red;
    if(_status == Status::dead)     currentColor = Qt::darkGray;

    setBrush(QBrush(currentColor));
}

void Citizen::updatePosition()
{
    auto currentPos = pos();

    QPointF nextPos = calculateNextPoint(currentPos);
    setPosition(nextPos);
}

void Citizen::updateStatus()
{
    const bool isInfected = (_status == Status::infected);
    const bool justInfected = isInfected && (_infectedTime == 0);
    if(justInfected)
        _infectedTime = _lifeCounter;

    if(isInfected)
        recover();
}

void Citizen::updateSpeed()
{
    double delta = _randomGenerator->bounded(1.0) - 0.5;

    const qreal newSpeed = _speed + delta;
    _speed = clamp(newSpeed, _minSpeed, _maxSpeed);
}

void Citizen::updateDirection()
{
    double delta = _randomGenerator->bounded(0.5) - 0.25;
    QRectF scenRect = scene()->sceneRect();
    QPointF position = pos();
    if(scenRect.contains(position))
        _direction += delta;

}

void Citizen::recover()
{
    const quint64 timeIll = _lifeCounter - _infectedTime;
    if(timeIll > _recoveryTime)
    {
        _status = Status::fit;
        _infectedTime = 0;
    }

    if(_randomGenerator->bounded(1.) < 0.005)
        _status = Status::dead;
}

QPointF Citizen::calculateNextPoint(const QPointF &start) const
{
    qreal dx = _speed * qSin(_direction);
    qreal dy = _speed * qCos(_direction);

    QPointF vector = QPointF(dx, dy);

    return start + vector;
}

void Citizen::setPosition(const QPointF &pos)
{
    const QRectF& sceneRect = scene()->sceneRect();
    if(pos.x() < sceneRect.x())
    { jumpWest(pos); return; }

    if(pos.x() > sceneRect.width())
    { jumpEast(pos); return;}

    if(pos.y() < sceneRect.y())
    { jumpSouth(pos); return;}

    if(pos.y() > sceneRect.height())
    { jumpNorth(pos); return;}

    setPos(pos);
}

void Citizen::jumpWest(const QPointF &pos)
{
    const double sceneWidth = scene()->sceneRect().width();
    QPointF newPos = pos + QPointF(sceneWidth, 0.0);
    setPos(newPos);
}

void Citizen::jumpEast(const QPointF &pos)
{
    const double sceneWidth = scene()->sceneRect().width();
    QPointF newPos = pos + QPointF(-sceneWidth, 0.0);
    setPos(newPos);
}

void Citizen::jumpNorth(const QPointF &pos)
{
    const double sceneHeight = scene()->sceneRect().height();
    QPointF newPos = pos + QPointF(0.0, -sceneHeight);
    setPos(newPos);
}

void Citizen::jumpSouth(const QPointF &pos)
{
    const double sceneHeight = scene()->sceneRect().height();
    QPointF newPos = pos + QPointF(0.0, sceneHeight);
    setPos(newPos);
}

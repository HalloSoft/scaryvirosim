#include "countvisuwidget.h"

#include <algorithm>
#include <QDebug>
#include <QPainter>

using namespace std;

CountVisuWidget::CountVisuWidget(QWidget *parent) :
    QWidget(parent)
{
    _color = QColor(Qt::black);

    QPalette pal;
    pal.setColor(QPalette::Background, Qt::white);
    this->setAutoFillBackground(true);
    this->setPalette(pal);
}

void CountVisuWidget::setColor(const QColor &color)
{
    _color = color;
}

void CountVisuWidget::reset()
{
    _values.clear();
    clearScreen();
}

void CountVisuWidget::appendValue(int value)
{
    _values.append(value);

    repaint();
}

void CountVisuWidget::paintEvent(QPaintEvent *)
{
    QPainter painter;
    painter.begin(this);

    painter.setPen(_color);

    drawValues(&painter);
}

void CountVisuWidget::drawValues(QPainter *painter) const
{
    int index = 0;
    for(int value : _values)
    {
        const int x = xScreenValue(index);
        const int y = yScreenValue(value);
        painter->drawLine(x, height(), x, y);
        ++index;
    }
}

int CountVisuWidget::xScreenValue(int rawValue) const
{
    int result = rawValue;

    if(_values.count() > width())
            result = rawValue * width() / _values.count();

    return result;
}

int CountVisuWidget::yScreenValue(int rawValue) const
{
    int max = *max_element(_values.begin(), _values.end());
    const int screenHeight = height();

    int result = 0;

    if(max > 0)
        result = screenHeight - (rawValue * screenHeight / max);

    return result;
}

void CountVisuWidget::clearScreen()
{
    QPainter painter;
    painter.begin(this);

    painter.eraseRect(rect());
    repaint();
}

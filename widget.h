#ifndef WIDGET_H
#define WIDGET_H



#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class WorldScene;

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

protected:
    void closeEvent(QCloseEvent* );

private slots:
    void setDeathsInfo(int number);
    void setInfectedInfo(int number);
    void reset();

private:
    void initializeConnections();
    void saveSettings() const;
    void readSettings();

    Ui::Widget*  ui;
    WorldScene* _world {nullptr};

    int _initialColumns {15};
    int _initialLines {20};
};
#endif // WIDGET_H

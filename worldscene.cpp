#include "worldscene.h"

#include "citizen.h"
#include "timeticker.h"

#include <QDebug>

WorldScene::WorldScene(QObject *parent) :
    QGraphicsScene(parent),
    _clock(new TimeTicker)
{
    Q_CHECK_PTR(_clock);

    establishConnections();

    setSceneRect(0, 0, 1200, 800);
    setBackgroundBrush(Qt::black);
    addRect(sceneRect(), QPen(Qt::gray, 1, Qt::DashDotLine));

}

void WorldScene::initialSpreadOfCitizen(int columns, int lines)
{
    if(lines < 1) lines = 1;
    if(columns < 1) columns = 1;

    clear();

    double lineHeight = sceneRect().height() / (lines + 1);
    double columnWidth = sceneRect().width() / (columns + 1);

    for(int line = 1; line <= lines; ++line)
    {
        double y = lineHeight * line;

        for(int column = 1; column <= columns; ++column)
        {
            double x = columnWidth * column;

            auto a = new Citizen;
            a->setVisible(true);
            a->setPos(x,y);
            addItem(a);
        }
    }
}

quint32 WorldScene::deadCitizen() const
{
    quint32 result = 0;

    for(auto citizen : citizen())
    {
        if(citizen->isDead())
           ++result;
    }

    return result;
}

quint32 WorldScene::infectedCitizen() const
{
    quint32 result = 0;

    for(auto citizen : citizen())
    {
        if(citizen->isInfected())
           ++result;
    }

    return result;
}

void WorldScene::run(bool value)
{
    Q_UNUSED(value);

    if(_clock)
        _clock->start();
}

void WorldScene::changeOnTick()
{
    for(auto citizen : citizen())
    {
        moveItem(citizen);
        interact(citizen);
    }

    emit deathsChanged(deadCitizen());

    const quint32 noOfInfected = infectedCitizen();
    emit infectedChanged(noOfInfected);

    if(noOfInfected == 0)
        _clock->stop();
}

CitizenList WorldScene::citizen() const
{
    CitizenList result;
    for(auto item : items())
    {
        auto citizen = dynamic_cast<Citizen*>(item);
        if(citizen)
            result << citizen;
    }

    return result;
}

void WorldScene::clear()
{
    for(auto item : items())
        removeItem(item);
}

void WorldScene::establishConnections()
{
    bool isConnected = false; Q_UNUSED(isConnected)
    isConnected = connect(_clock, &TimeTicker::tick, this, &WorldScene::changeOnTick);
    Q_ASSERT(isConnected);
}

void WorldScene::moveItem(Citizen *item)
{
    if(item)
        item->update();
}

void WorldScene::interact(Citizen *item)
{
    for(auto collitionItem :  collidingItems(item))
    {
        auto citizen = dynamic_cast<Citizen*>(collitionItem);
        if(citizen)
            citizen->giveVirus(item);
    }
}

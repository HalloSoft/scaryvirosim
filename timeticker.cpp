#include "timeticker.h"

#include <chrono>
#include <QDebug>

using namespace std::chrono_literals;

TimeTicker::TimeTicker(QObject *parent) :
    QObject(parent)
{
}

quint64 TimeTicker::ticks() const
{
    return _count;
}

void TimeTicker::start()
{
    _timerId = startTimer(100ms);
}

void TimeTicker::stop()
{
    killTimer(_timerId);
}

void TimeTicker::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event)

    emit tick();
    ++_count;
}

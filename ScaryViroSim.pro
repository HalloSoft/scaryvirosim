VERSION_MAJOR = 2
VERSION_MINOR = 0
VERSION_REVISION = 0
VERSION_BUILD = 4


QT       += core gui widgets

CONFIG += c++17

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += "VERSION_MAJOR=$$VERSION_MAJOR"\
           "VERSION_MINOR=$$VERSION_MINOR"\
           "VERSION_REVISION=$$VERSION_REVISION"\
           "VERSION_BUILD=$$VERSION_BUILD"

SOURCES += \
    citizen.cpp \
    countvisuwidget.cpp \
    main.cpp \
    timeticker.cpp \
    widget.cpp \
    worldscene.cpp

HEADERS += \
    citizen.h \
    countvisuwidget.h \
    timeticker.h \
    widget.h \
    worldscene.h

FORMS += \
    widget.ui

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_REVISION}.$${VERSION_BUILD}

RESOURCES += \
    resource.qrc

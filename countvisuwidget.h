#ifndef COUNTVISUWIDGET_H
#define COUNTVISUWIDGET_H

#include <QWidget>

class CountVisuWidget : public QWidget
{
    Q_OBJECT
public:
    CountVisuWidget(QWidget* parent);
    void setColor(const QColor& color);
    void reset();

public slots:
    void appendValue(int value);

protected:
    void paintEvent(QPaintEvent*);

private:
    void drawValues(QPainter* painter) const;
    int xScreenValue(int rawValue) const;
    int yScreenValue(int rawValue) const;
    void clearScreen();

    QColor _color;
    QVector<int> _values;

};

#endif // COUNTVISUWIDGET_H
